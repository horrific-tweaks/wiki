`minecraft.horrific.dev` is a simple vanilla Minecraft server hosted on [atrocious](https://atrocious.horrific.dev).

## Joining the Server

If you have not been invited, you need to contact an existing member to request an invitation. This server is not open to the public, and access is not guaranteed.

Once your request to join the server has been accepted, you can log into the server from the Minecraft Java Edition title screen by clicking "Multiplayer", then "Add Server", and entering "minecraft.horrific.dev" as the server address.

Note: the server is currently running version 1.18.

### Resource Pack

By default, a resource pack should be automatically applied when you join the server. It is recommended to keep this enabled, as it will provide a much better experience with some of our datapacks.

If the automatic download is causing problems, you can [install it manually](./Installing) - however, you will need to update it if we make any significant changes. The resource pack can be downloaded [here](https://minecraft.horrific.dev/horrific.zip).

### Useful Things

See the [Useful Things](./Useful-Things) page for a list of resources to improve your gameplay.

## Plugins & Datapacks

The server has a few small modifications to make the game a bit more interesting to play. However, we will not install any plugins that change the world generation or core gameplay in order to make it easier to update the world to new Minecraft versions.

- **Plugins (Fabric)**
    - Performance:
        - [Lithium](https://modrinth.com/mod/lithium)
        - [Starlight](https://modrinth.com/mod/starlight)
        - [Krypton](https://modrinth.com/mod/krypton)
        - [ServerCore](https://modrinth.com/mod/servercore)
        - [LazyDFU](https://modrinth.com/mod/lazydfu)
        - [Alternate Current](https://modrinth.com/mod/alternate-current)
        - [Dimensional Threading](https://github.com/WearBlackAllDay/DimensionalThreading)
        - [FerriteCore](https://modrinth.com/mod/ferrite-core)
        - [C2ME](https://modrinth.com/mod/c2me-fabric)
        - [Chunky Pregenerator](https://www.curseforge.com/minecraft/mc-mods/chunky-pregenerator)
    - [Discord Integration](https://modrinth.com/mod/dcintegration)
    - [FabricTPA](https://www.curseforge.com/minecraft/mc-mods/fabrictpa)
    - [KeepHeadNames](https://modrinth.com/mod/keepheadnames)
    - [Ledger](https://modrinth.com/mod/ledger)
- **Plugins (Paper)**
    - [MultiRequests](https://dev.bukkit.org/projects/multirequests) (/tpa commands, etc)
    - [Chairs](https://dev.bukkit.org/projects/chairsreloaded) (right click on stair blocks with an empty hand to sit)
    - [SafeSpectate](https://www.curseforge.com/minecraft/bukkit-plugins/safespectate) (`/spectate` command to toggle spectator mode)
    - [BannerLayer](https://www.spigotmc.org/resources/bannerlayer.90013/) (enables banner crafting beyond the 6-layer limit)
    - [WeatherRestrictions](https://www.spigotmc.org/resources/weatherrestrictions.7619/) (prevents thunderstorms as they happen far too frequently)
    - [DiscordSRV](https://dev.bukkit.org/projects/discordsrv) (connects in-game chat to our discord channel)
- **Datapacks**
    - [vanillatweaks.net](https://vanillatweaks.net)
        - Armor Statues - see: [Tutorial Video](https://youtu.be/nV9-_RacnoI?t=145)
        - Multiplayer Sleep (only one person needs to sleep for night to pass)
        - More Mob Heads
        - Player Head Drops (modified: only drops when the player is being roasted over a fire)
        - Wandering Trades - Blocks Only
        - UniversalDyeing (allows crafting with any dyed block, e.g. crafting purple glass from green glass)
        - Anti Enderman Grief
        - Silence Mobs - name a mob "silence me" with a [nametag](https://minecraft.fandom.com/wiki/Name_Tag) to silence it
        - Nether Portal Coords - `/trigger nc_inNether` or `/trigger nc_inOverworld`
        - Coordinates HUD - `/trigger ch_toggle` to show coords
        - Track Raw Statistics
        - AFK Display
    - bac_advancements ([BlazeAndCave](https://www.planetminecraft.com/data-pack/blazeandcave-s-advancements-pack-1-12/))
    - [Horrific Tweaks](https://minecraft.horrific.dev)
        - Wizard Staff - see: [Tutorial Video](https://youtu.be/JoTcxrZ2nu4)
        - Cherry Trees
        - More Berries
        - Cat Ears
        - Killer Bunnies
        - Bean Cans
        - Iscream Floats
        - Petrified Oak
        - Bugle Horns
        - Pumpkin Hats
    - [Custom Roleplay Data](https://www.curseforge.com/minecraft/customization/custom-roleplay-data-datapack) (use `/trigger CustomModelData set ###` to change an item's model number)
- **Gamerules & Config**
    - [gamerules](https://minecraft.fandom.com/wiki/Game_rule)
        - doFireTick: false
        - keepInventory: true
    - [paper.yml](https://paper.readthedocs.io/en/latest/server/configuration.html)
        - unsupported-settings:
            - allow-permanent-block-break-exploits: true
        - keep-spawn-loaded: false
        - update-pathfinding-on-block-update: false
        - use-faster-eigencraft-redstone: true
        - optimize-explosions: true
        - per-player-mob-spawns: true
    - [purpur.yml](https://purpur.pl3x.net/docs/Configuration/)
        - blocks.slab.break-individual-slabs-when-sneaking: true
        - item.glow_berries.eat-glow-duration: 300

## Code of Conduct

This server inherits the [Horrific.Dev Code of Conduct](https://horrific.dev/wiki/Code-of-Conduct).