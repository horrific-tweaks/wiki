[vanillatweaks.net](https://vanillatweaks.net) includes many small non-breaking modifications to the game, such as resource packs that subtly change block/item textures.

[Fabric](https://fabricmc.net/) is a Minecraft mod loader/toolchain that allows other plugins to be added to your client:
- [Krypton](https://github.com/astei/krypton/releases) improves Minecraft's networking performance and uses the system zlib on Linux
- [Sodium](https://github.com/CaffeineMC/sodium-fabric/releases) is a replacement rendering engine focused strictly on performance.
- [LambDynamicLights](https://www.curseforge.com/minecraft/mc-mods/lambdynamiclights) adds various dynamic lighting effects.
- [Enhanced Block Entities](https://modrinth.com/mod/ebe) replaces block entities (such as chests) with static block models to improve performance
    - caveat: the chest open/close animation flickers a bit when swapping models
- [Ok Zoomer](https://www.curseforge.com/minecraft/mc-mods/ok-zoomer) adds an optifine-like zoom key
- [Camera Utilities](https://www.curseforge.com/minecraft/mc-mods/camera-utils) allows more specific camera repositioning, detaching, and other features.
- [ShulkerBoxTooltip](https://www.curseforge.com/minecraft/mc-mods/shulkerboxtooltip) provides a preview of shulker box contents when viewing them in the inventory.